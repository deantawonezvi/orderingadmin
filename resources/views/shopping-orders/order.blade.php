@extends('layouts.app')

@section('content')
    @include('partials.home_menu.menu')
    <div class="col-sm-12">
        <div class="card-large card-default card-body">
            <h3 class="left"><img src="{{asset('css/social-icons/delivery-note.svg')}}" alt="clipboard"
                                  width="15%">Shopping Order #{{$order->id}}</h3>
            <br><br>
            <hr>
            <h4>
                Customer Details
            </h4>
            <p style="font-size: larger; font-family: CamptonLight">
                Customer Name - <span style="font-weight: bolder">{{$order->name}} ({{$order->email_address}}
                    ) ({{$order->cell_number}})</span><br>
                Delivery Address - <span style="font-weight: bolder">{{$order->delivery_address}}
                    ({{$order->delivery_town}})</span>
            </p>
            <br>
            <hr>
            <h4>
                Order Details
            </h4>
            <p style="font-size: larger; font-family: CamptonLight">
                Total - <span style="font-weight: bolder">${{$order->total}}</span><br>
                Payment Status - <span style="font-weight: bolder">{{$order->payment_status}}</span><br>
                Order Status - <span style="font-weight: bolder">{{$order->status}}</span><br>
            </p>
            <h5>
                <u>Order Items</u>
            </h5>
            @foreach($order_details as $detail)
                <p style="font-size: larger; font-family: CamptonLight">
                    Name - <span style="font-weight: bolder">{{$detail->name}}</span><br>
                    Price - <span style="font-weight: bolder">${{$detail->price}}</span><br>
                    Quantity - <span style="font-weight: bolder">{{$detail->quantity}}</span><br>
                </p>
                <hr>
            @endforeach


        </div>
        <br><br>

    </div>

@endsection
