<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use Uuids;

    protected $guarded = [];
}
