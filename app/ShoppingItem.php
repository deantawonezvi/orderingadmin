<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class ShoppingItem extends Model implements HasMedia
{
    use HasMediaTrait, Uuids;
    protected $guarded = [];

}
