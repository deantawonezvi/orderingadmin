<?php

namespace App\Http\Controllers;

use App\Customer;
use App\ShoppingOrder;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;

class ShoppingOrderController extends Controller
{
    public function index(){
        $orders = ShoppingOrder::paginate(10);

        $this->checkShoppingPayments();

        return view('shopping-orders.orders', compact('orders', $orders));
    }


    public function checkShoppingPayments(){
        $client = new GuzzleHttp\Client();

        $res = $client->get(env('APP_URL') . '/paynow/check');
        $res->getBody()->getContents();

    }


    public function viewOrder($id){
        $order = ShoppingOrder::find($id);

        if (empty($order)) {
            abort(404);
        };

        /*$order_details = ShoppingOrderDetails::whereShoppingOrderId($id)
                                                ->get();*/


        $order_details = DB::table('shopping_order_details')
            ->where('shopping_order_details.order_id', '=', $id)
            ->join('shopping_items', 'shopping_items.id', '=', 'shopping_order_details.shopping_item_id')
            ->select('shopping_order_details.quantity','shopping_items.name','shopping_items.price')
            ->get();

        $data = ['order' => $order, 'order_details' => $order_details];

        return view('shopping-orders.order')->with($data);
    }
}
