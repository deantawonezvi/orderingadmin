<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use Uuids;
    protected $table = 'users';
    protected $hidden = [
        'password', 'remember_token','token','created_at','updated_at','verified','status'
    ];

}
